import grpc
from enum import Enum
from google.protobuf import empty_pb2

from PySide6 import QtCore
import re

from assistant_rpc import assistant_pb2
from assistant_rpc import assistant_pb2_grpc

DATETIME_FORMAT = 'yyyy-MM-dd HH:mm:ss.z'

class Row():
    def __init__(self, grpc_row) -> None:
        # allow no more than 3 digits after decomal point in seconds
        str = re.sub('(:\d{2}\.\d{3}).*$', '\\1', grpc_row.date)
        date = QtCore.QDateTime.fromString(str, DATETIME_FORMAT)
        # convert from UTC to local timezone
        date.setTimeSpec(QtCore.Qt.UTC)
        self.date = date.toLocalTime()

        self.message_id = grpc_row.message_id
        self.text = grpc_row.text
        self.deadline = grpc_row.deadline


class MessageType(Enum):
    READ = 0
    WRITE = 1


class AssistantClient():
    def __init__(self):
        self.channel = grpc.insecure_channel('localhost:5005')
        self.stub = assistant_pb2_grpc.AssistServerStub(self.channel)

    def close(self):
        self.channel.close()

    def get_user_list(self):
        response = self.stub.GetUserList(empty_pb2.Empty())
        return response

    def create_user(self, name):
        user = assistant_pb2.User()
        user.name = name
        response = self.stub.CreateUser(user)

    def process_user_input(self, user, text, count, deadline: QtCore.QDateTime = None, forcedAction = None):
        user_input = assistant_pb2.UserInput()
        user_input.user = user
        user_input.text = text
        user_input.count = count
        if deadline:
            user_input.deadline = deadline.toString(DATETIME_FORMAT)
        if count < 0:
            raise Exception("count should be more than 0")

        if (forcedAction == MessageType.READ):
            user_input.forcedAction = assistant_pb2.UserInput.READ
        elif (forcedAction == MessageType.WRITE):
            user_input.forcedAction = assistant_pb2.UserInput.WRITE
        else:
            user_input.forcedAction = assistant_pb2.UserInput.AUTO

        response = self.stub.ProcessUserInput(user_input)
        rows = [Row(x) for x in response.rows]

        action = MessageType.READ
        if response.action == assistant_pb2.Action.READ:
            action = MessageType.READ
        elif response.action == assistant_pb2.Action.WRITE:
            action = MessageType.WRITE
        else:
            raise Exception("response.action must be READ or WRITE")

        return rows, response.status, action

    def latest_recorded_messages(self, user, count):
        latest_msg_request = assistant_pb2.LatestMsgsRequest()
        latest_msg_request.user = user
        latest_msg_request.count = count
        response = self.stub.LatestRecordedMessages(latest_msg_request)
        rows = [Row(x) for x in response.rows]
        return rows, response.status

    def type_read_or_write(self, text):
        user_input = assistant_pb2.UserInput()
        user_input.text = text
        response = self.stub.GetTypeQA(user_input)

        if response.action == assistant_pb2.Action.READ:
            return MessageType.READ, response.status
        elif response.action == assistant_pb2.Action.WRITE:
            return MessageType.WRITE, response.status
        else:
            raise Exception("response.action must be assistant_pb2.Type.READ or assistant_pb2.Type.WRITE")
