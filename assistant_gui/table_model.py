from PySide6 import QtCore

class TableModel(QtCore.QAbstractTableModel):
    def __init__(self, parent) -> None:
        super().__init__(parent)

        self.rows = []
        self.headers = ["date", "id", "text", "deadline"]

    def rowCount(self, parent) -> int:
        return len(self.rows)

    def columnCount(self, parent) -> int:
        return len(self.headers)

    def data(self, index, role):
        if role == QtCore.Qt.DisplayRole:
            v = self.__getValue(index)
            if index.column() == 0:
                fmt = "d.MM.yy hh:mm:ss"
                v = v.toString(fmt)
            return v
        if role == QtCore.Qt.UserRole:
            return self.__getValue(index)

    def __getValue(self, index):
        v = ""
        row = self.rows[index.row()]
        if index.column() == 0:
            v = row.date
        elif index.column() == 1:
            v = row.message_id
        elif index.column() == 2:
            v = row.text
        elif index.column() == 3:
            v = row.deadline
        return v

    def headerData(self, section: int, orientation, role: int = ...):
        if role == QtCore.Qt.DisplayRole and orientation == QtCore.Qt.Horizontal:
            return self.headers[section]
        return super().headerData(section, orientation, role)

    def refreshData(self, newData):
        self.layoutAboutToBeChanged.emit()
        self.rows = newData
        self.layoutChanged.emit()
