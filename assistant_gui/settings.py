import json
from pathlib import Path

class DefaultConfig:
    user = ""
    most_recent_records = 5


class Config:
    def __init__(self, path) -> None:
        self.file_name = path
        try:
            with open(path) as f:
                jdata = json.load(f)

            self.user = jdata.get("user", DefaultConfig.user)
            self.most_recent_records = jdata.get("most_recent_records", DefaultConfig.most_recent_records)
            if (self.most_recent_records < 5):
                self.most_recent_records = 5
        except:
            self.user = DefaultConfig.user
            self.most_recent_records = DefaultConfig.most_recent_records
    
    def save(self):
        data = {}
        data["user"] = self.user
        data["most_recent_records"] = self.most_recent_records

        dir = self.file_name.parent
        Path(dir).mkdir(parents=True, exist_ok=True)
        with open(self.file_name, 'w', encoding='utf-8') as f:
            json.dump(data, f, indent=4)
