# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'main.ui'
##
## Created by: Qt User Interface Compiler version 6.2.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QAction, QBrush, QColor, QConicalGradient,
    QCursor, QFont, QFontDatabase, QGradient,
    QIcon, QImage, QKeySequence, QLinearGradient,
    QPainter, QPalette, QPixmap, QRadialGradient,
    QTransform)
from PySide6.QtWidgets import (QApplication, QCheckBox, QDateTimeEdit, QFrame,
    QHBoxLayout, QHeaderView, QLabel, QLineEdit,
    QMainWindow, QMenu, QMenuBar, QPushButton,
    QSizePolicy, QSpacerItem, QTableView, QVBoxLayout,
    QWidget)
from assistant_gui import icons_rc

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(790, 609)
        icon = QIcon()
        icon.addFile(u":/main.svg", QSize(), QIcon.Normal, QIcon.On)
        MainWindow.setWindowIcon(icon)
        self.actionDefault = QAction(MainWindow)
        self.actionDefault.setObjectName(u"actionDefault")
        self.actionDefault.setCheckable(True)
        self.actionDefault.setChecked(True)
        self.actionEmerald = QAction(MainWindow)
        self.actionEmerald.setObjectName(u"actionEmerald")
        self.actionEmerald.setCheckable(True)
        self.actionEmerald.setChecked(False)
        self.actionEmerald.setEnabled(False)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.centralwidget.sizePolicy().hasHeightForWidth())
        self.centralwidget.setSizePolicy(sizePolicy)
        self.verticalLayout = QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(-1, 3, -1, 3)
        self.frame = QFrame(self.centralwidget)
        self.frame.setObjectName(u"frame")
        self.frame.setMinimumSize(QSize(0, 35))
        self.frame.setFrameShape(QFrame.NoFrame)
        self.frame.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_2 = QHBoxLayout(self.frame)
        self.horizontalLayout_2.setSpacing(2)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.horizontalLayout_2.setContentsMargins(1, 1, 1, 1)
        self.home = QPushButton(self.frame)
        self.home.setObjectName(u"home")
        self.home.setMinimumSize(QSize(0, 35))
        self.home.setMaximumSize(QSize(35, 35))
        font = QFont()
        font.setPointSize(12)
        font.setBold(True)
        self.home.setFont(font)
        self.home.setMouseTracking(True)
        self.home.setFocusPolicy(Qt.NoFocus)
        self.home.setToolTipDuration(3000)
        icon1 = QIcon()
        icon1.addFile(u":/home.svg", QSize(), QIcon.Normal, QIcon.On)
        self.home.setIcon(icon1)
        self.home.setIconSize(QSize(20, 20))

        self.horizontalLayout_2.addWidget(self.home)

        self.clearUserInput = QPushButton(self.frame)
        self.clearUserInput.setObjectName(u"clearUserInput")
        self.clearUserInput.setMinimumSize(QSize(35, 35))
        self.clearUserInput.setMaximumSize(QSize(35, 35))
        self.clearUserInput.setMouseTracking(False)
        self.clearUserInput.setFocusPolicy(Qt.NoFocus)
        icon2 = QIcon()
        icon2.addFile(u":/backspace.svg", QSize(), QIcon.Normal, QIcon.On)
        self.clearUserInput.setIcon(icon2)
        self.clearUserInput.setIconSize(QSize(20, 20))

        self.horizontalLayout_2.addWidget(self.clearUserInput)

        self.exportRows = QPushButton(self.frame)
        self.exportRows.setObjectName(u"exportRows")
        self.exportRows.setMinimumSize(QSize(35, 35))
        self.exportRows.setMaximumSize(QSize(35, 35))
        icon3 = QIcon()
        icon3.addFile(u":/document.svg", QSize(), QIcon.Normal, QIcon.On)
        self.exportRows.setIcon(icon3)
        self.exportRows.setIconSize(QSize(20, 20))

        self.horizontalLayout_2.addWidget(self.exportRows)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer)

        self.settings = QPushButton(self.frame)
        self.settings.setObjectName(u"settings")
        self.settings.setMinimumSize(QSize(35, 35))
        self.settings.setMaximumSize(QSize(35, 35))
        icon4 = QIcon()
        icon4.addFile(u":/settings.svg", QSize(), QIcon.Normal, QIcon.On)
        self.settings.setIcon(icon4)
        self.settings.setIconSize(QSize(20, 20))

        self.horizontalLayout_2.addWidget(self.settings)


        self.verticalLayout.addWidget(self.frame)

        self.widget = QWidget(self.centralwidget)
        self.widget.setObjectName(u"widget")
        sizePolicy.setHeightForWidth(self.widget.sizePolicy().hasHeightForWidth())
        self.widget.setSizePolicy(sizePolicy)
        self.widget.setMinimumSize(QSize(0, 50))
        self.widget.setMaximumSize(QSize(16777215, 50))
        self.widget.setAutoFillBackground(False)
        self.horizontalLayout = QHBoxLayout(self.widget)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(0, 0, 0, 5)
        self.userInput = QLineEdit(self.widget)
        self.userInput.setObjectName(u"userInput")
        font1 = QFont()
        font1.setPointSize(12)
        self.userInput.setFont(font1)

        self.horizontalLayout.addWidget(self.userInput)

        self.find = QPushButton(self.widget)
        self.find.setObjectName(u"find")
        self.find.setEnabled(True)
        self.find.setMaximumSize(QSize(100, 35))
        self.find.setFont(font1)
        icon5 = QIcon()
        icon5.addFile(u":/search.svg", QSize(), QIcon.Normal, QIcon.On)
        self.find.setIcon(icon5)
        self.find.setIconSize(QSize(20, 20))
        self.find.setAutoDefault(True)

        self.horizontalLayout.addWidget(self.find)

        self.save = QPushButton(self.widget)
        self.save.setObjectName(u"save")
        self.save.setMaximumSize(QSize(16777215, 35))
        self.save.setFont(font1)
        icon6 = QIcon()
        icon6.addFile(u":/save.svg", QSize(), QIcon.Normal, QIcon.On)
        self.save.setIcon(icon6)
        self.save.setIconSize(QSize(20, 20))

        self.horizontalLayout.addWidget(self.save)


        self.verticalLayout.addWidget(self.widget)

        self.widget_2 = QWidget(self.centralwidget)
        self.widget_2.setObjectName(u"widget_2")
        self.widget_2.setMinimumSize(QSize(0, 35))
        self.horizontalLayout_3 = QHBoxLayout(self.widget_2)
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.horizontalLayout_3.setContentsMargins(0, 0, 0, 5)
        self.deadlineValidity = QCheckBox(self.widget_2)
        self.deadlineValidity.setObjectName(u"deadlineValidity")
        self.deadlineValidity.setFont(font1)

        self.horizontalLayout_3.addWidget(self.deadlineValidity)

        self.deadline = QDateTimeEdit(self.widget_2)
        self.deadline.setObjectName(u"deadline")
        self.deadline.setFont(font1)
        self.deadline.setCalendarPopup(True)

        self.horizontalLayout_3.addWidget(self.deadline)

        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_3.addItem(self.horizontalSpacer_2)


        self.verticalLayout.addWidget(self.widget_2)

        self.label = QLabel(self.centralwidget)
        self.label.setObjectName(u"label")
        sizePolicy.setHeightForWidth(self.label.sizePolicy().hasHeightForWidth())
        self.label.setSizePolicy(sizePolicy)
        self.label.setMinimumSize(QSize(772, 50))
        self.label.setMaximumSize(QSize(16777215, 50))
        self.label.setFont(font1)
        self.label.setMouseTracking(False)
        self.label.setFocusPolicy(Qt.TabFocus)
        self.label.setLineWidth(3)
        self.label.setTextFormat(Qt.AutoText)
        self.label.setScaledContents(False)
        self.label.setWordWrap(True)
        self.label.setMargin(1)
        self.label.setIndent(-1)

        self.verticalLayout.addWidget(self.label)

        self.tableView = QTableView(self.centralwidget)
        self.tableView.setObjectName(u"tableView")
        self.tableView.setMinimumSize(QSize(772, 0))
        self.tableView.setFont(font1)

        self.verticalLayout.addWidget(self.tableView)

        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(MainWindow)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 790, 22))
        self.menuColors = QMenu(self.menubar)
        self.menuColors.setObjectName(u"menuColors")
        MainWindow.setMenuBar(self.menubar)

        self.menubar.addAction(self.menuColors.menuAction())
        self.menuColors.addAction(self.actionDefault)
        self.menuColors.addAction(self.actionEmerald)

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"Assistant", None))
        self.actionDefault.setText(QCoreApplication.translate("MainWindow", u"Default", None))
        self.actionEmerald.setText(QCoreApplication.translate("MainWindow", u"Emerald", None))
#if QT_CONFIG(tooltip)
        self.home.setToolTip(QCoreApplication.translate("MainWindow", u"Home: revert to initial state", None))
#endif // QT_CONFIG(tooltip)
        self.home.setText("")
#if QT_CONFIG(tooltip)
        self.clearUserInput.setToolTip(QCoreApplication.translate("MainWindow", u"Clear input field", None))
#endif // QT_CONFIG(tooltip)
        self.clearUserInput.setText("")
        self.exportRows.setText("")
        self.settings.setText("")
        self.find.setText(QCoreApplication.translate("MainWindow", u"Find", None))
        self.save.setText(QCoreApplication.translate("MainWindow", u"Save", None))
        self.deadlineValidity.setText(QCoreApplication.translate("MainWindow", u"deadline", None))
        self.deadline.setDisplayFormat(QCoreApplication.translate("MainWindow", u"d.M.yyyy H:mm", None))
        self.label.setText("")
        self.menuColors.setTitle(QCoreApplication.translate("MainWindow", u"Colors", None))
    # retranslateUi

