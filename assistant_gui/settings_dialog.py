import copy
from PySide6 import QtWidgets, QtCore, QtGui
from assistant_gui import ui_settings
from assistant_gui import settings


class ConfigurationUI(QtWidgets.QDialog):
    settings_update = QtCore.Signal(settings.Config)

    def __init__(self, cfg : settings.Config):

        super(ConfigurationUI, self).__init__()
        self.ui = ui_settings.Ui_Settings()
        self.ui.setupUi(self)
        self.settings = copy.deepcopy(cfg)

        self.ui.buttonBox.button(QtWidgets.QDialogButtonBox.Ok).setEnabled(False)

        filter = QtCore.QRegularExpression("[0-9a-zA-Z_ \-]*")
        validator = QtGui.QRegularExpressionValidator(filter, self)
        self.ui.user_name.setValidator(validator)

        self.ui.user_name.textChanged.connect(self.set_ok)
        self.ui.user_name.setText(self.settings.user)
        self.ui.most_recent_records.setValue(self.settings.most_recent_records)

        if self.ui.user_name.text():
            self.ui.user_name.setEnabled(False)

        self.accepted.connect(self.on_ok)

    @QtCore.Slot()
    def set_ok(self):
        status = self.ui.user_name.text() != ""
        self.ui.buttonBox.button(QtWidgets.QDialogButtonBox.Ok).setEnabled(status)

    @QtCore.Slot()
    def on_ok(self):
        self.settings.most_recent_records = self.ui.most_recent_records.value()
        self.settings.user = self.ui.user_name.text()
        self.settings_update.emit(self.settings)

    def reject(self) -> None:
        if self.settings.user:
            return super().reject()
        return None
