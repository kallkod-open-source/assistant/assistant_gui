import sys
import os
from pathlib import Path
from PySide6.QtWidgets import QApplication, QMainWindow, QHeaderView, QDialogButtonBox
from PySide6 import QtCore
from PySide6.QtGui import QActionGroup, QShortcut

from assistant_gui.mainwindow import Ui_MainWindow
from assistant_gui.assistant_client import AssistantClient, MessageType
from assistant_gui import settings
from assistant_gui import __path__ as package_path
from assistant_gui import settings_dialog
from assistant_gui.table_model import TableModel
from assistant_gui.export import DataExporter


def find_user(user_list, name):
    for u in user_list.users:
        if u.name == name:
            return True
    return False


class MainWindow(QMainWindow):
    def __init__(self, settings):
        super(MainWindow, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.settings = settings
        self.client = AssistantClient()
        self.model = TableModel(None)

        self.proxyModel = QtCore.QSortFilterProxyModel(None);
        self.proxyModel.setSortRole(QtCore.Qt.UserRole)
        self.proxyModel.setSourceModel(self.model)
        self.ui.tableView.setModel(self.proxyModel)
        self.ui.tableView.setSortingEnabled(True)
        self.ui.tableView.horizontalHeader().setSectionResizeMode(2, QHeaderView.Stretch)

        self.ui.find.clicked.connect(self.find)
        self.ui.save.clicked.connect(self.save)
        self.ui.userInput.returnPressed.connect(self.processMessage)

        self.ui.home.clicked.connect(self.home)
        self.homeKey = QShortcut("Alt+H", self)
        self.homeKey.activated.connect(self.ui.home.click)

        self.colorActions = QActionGroup(None)
        self.colorActions.addAction(self.ui.actionDefault)
        self.colorActions.addAction(self.ui.actionEmerald)

        self.ui.actionDefault.triggered.connect(self.defaultColors)
        self.ui.actionEmerald.triggered.connect(self.emeraldColors)

        self.getLatestRecordedMessages(self.settings.most_recent_records)

        self.timer = QtCore.QTimer()
        self.timer.setSingleShot(True)
        self.timer.timeout.connect(self.preview)
        self.ui.userInput.textChanged.connect(self.launchTimer)

        self.ui.clearUserInput.clicked.connect(self.clearInput)
        self.clearKey = QShortcut("Alt+C", self)
        self.clearKey.activated.connect(self.ui.clearUserInput.click)

        self.ui.settings.clicked.connect(self.configure)

        self.exporter = DataExporter(self, self.proxyModel)
        self.ui.exportRows.clicked.connect(self.exporter.export)

        self.ui.deadline.setDateTime(QtCore.QDateTime.currentDateTime())
        self.ui.deadline.dateTimeChanged.connect(self.deadlineChanged)

    @QtCore.Slot()
    def home(self):
        self.getLatestRecordedMessages(self.settings.most_recent_records)

    def getLatestRecordedMessages(self, count):
        rows, status = self.client.latest_recorded_messages(self.settings.user, count)
        self.model.refreshData(rows)
        self.ui.label.setText(status)
        self.ui.tableView.resizeColumnsToContents()

    @QtCore.Slot()
    def preview(self):
        text = self.ui.userInput.text().strip()
        action_rw, status = self.client.type_read_or_write(text)
        if action_rw == MessageType.WRITE:
            self.getLatestRecordedMessages(self.settings.most_recent_records)
        self.ui.label.setText(status)

    @QtCore.Slot()
    def launchTimer(self, arg):
        self.timer.start(1500)

    def __processMessage(self, forcedAction):
        text = self.ui.userInput.text().strip()
        dt = None
        if self.ui.deadlineValidity.isChecked():
            dt = self.ui.deadline.dateTime()
        rows, status, action = self.client.process_user_input(self.settings.user, text,
            self.settings.most_recent_records, dt, forcedAction)
        self.model.refreshData(rows)
        self.ui.label.setText(status)
        self.ui.tableView.resizeColumnsToContents()
        self.timer.stop()
        if action == MessageType.WRITE:
            self.ui.userInput.setText("")

    @QtCore.Slot()
    def processMessage(self):
        self.__processMessage(None)

    @QtCore.Slot()
    def find(self):
        self.__processMessage(MessageType.READ)

    @QtCore.Slot()
    def save(self):
        if self.ui.userInput.text().strip():
            self.__processMessage(MessageType.WRITE)

    @QtCore.Slot()
    def defaultColors(self):
        app.setStyleSheet("")

    @QtCore.Slot()
    def emeraldColors(self):
        app.setStyleSheet(loadCSS())

    @QtCore.Slot()
    def clearInput(self):
        self.ui.userInput.setText("")

    @QtCore.Slot()
    def configure(self):
        dialog = settings_dialog.ConfigurationUI(self.settings)
        dialog.setModal(True)
        dialog.settings_update.connect(self.update_settings)
        dialog.exec()

    @QtCore.Slot(settings.Config)
    def update_settings(self, cfg : settings.Config):
        self.settings = cfg
        self.settings.save()
        self.getLatestRecordedMessages(self.settings.most_recent_records)

        user_list = self.client.get_user_list()
        if not find_user(user_list, self.settings.user):
            self.client.create_user(self.settings.user)

    @QtCore.Slot(QtCore.QDateTime)
    def deadlineChanged(self, dt: QtCore.QDateTime):
        self.ui.deadlineValidity.setChecked(dt > QtCore.QDateTime.currentDateTime())


def loadCSS(fileName = "main.css"):
    """Load CSS from the given file"""
    path = os.path.join(package_path[0], fileName)
    f = QtCore.QFile(path)
    f.open(QtCore.QFile.ReadOnly);
    css = f.readAll().data().decode()
    f.close()
    return css


if __name__ == "__main__":
    app = QApplication(sys.argv)

    settings_path = Path.home() / ".config" / "assistant" / "gui" / "config.json"
    settings = settings.Config(settings_path)
    window = MainWindow(settings)

    while not settings_path.is_file():
        dialog = settings_dialog.ConfigurationUI(settings)
        dialog.ui.buttonBox.button(QDialogButtonBox.Cancel).setEnabled(False)
        dialog.settings_update.connect(window.update_settings)
        dialog.exec()

    window.show()

    sys.exit(app.exec())
