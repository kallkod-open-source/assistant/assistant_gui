from PySide6.QtWidgets import QFileDialog
from PySide6 import QtCore, QtGui, QtPrintSupport
from pathlib import Path

class DataExporter(QtCore.QObject):
    HTML = """
<!doctype html>
<html>
<head>
<title>Assistant records</title>
    <style>
    table, th, td {{ border: 1px solid lightgrey; border-collapse: collapse; padding: 10px; }}
    .row_even {{ background-color: #F8F8F8; }}
    </style>
</head>
<body>
{}
</body>
</html>
"""

    def __init__(self, parent, data : QtCore.QSortFilterProxyModel) -> None:
        super().__init__(parent)
        self.model = data

    def __generateHTML(self):
        content = """
<table>
<thead>
    <tr class="header">
    <th>Date</th>
    <th>Text</th>
    <th>Deadline</th>
    </thead>
</tr>
"""
        content+= "<tbody>\n"
        count = self.model.rowCount(QtCore.QModelIndex())
        for row_idx in range(0, count):
            row_class = "row_odd" if row_idx % 2 else "row_even"
            content += f'<tr class="{row_class}">\n\t'
            for columnm_idx in [0, 2, 3]:
                index = self.model.index(row_idx, columnm_idx, QtCore.QModelIndex())
                src_idx = self.model.mapToSource(index)
                value = self.model.sourceModel().data(src_idx, QtCore.Qt.DisplayRole)
                content += f'<td>{value}</td> '
            content += "\n</tr>\n"
        content+= "</tbody>\n</table>\n"

        return content

    @QtCore.Slot(str)
    def writeData(self, name):
        doc = QtGui.QTextDocument()
        body = self.__generateHTML()
        doc.setHtml(self.HTML.format(body))
        printer = QtPrintSupport.QPrinter()
        printer.setOutputFormat(QtPrintSupport.QPrinter.PdfFormat)
        printer.setOutputFileName(name)
        doc.print_(printer)

    @QtCore.Slot()
    def export(self):
        dialog = QFileDialog(self.parent(), "Save file", str(Path.home() / "Documents"), "PDF (*.pdf);;All files (*)")
        dialog.setDefaultSuffix(".pdf")
        dialog.setModal(True)
        dialog.setAcceptMode(QFileDialog.AcceptMode.AcceptSave)
        dialog.fileSelected.connect(self.writeData)
        dialog.open()
        return None
