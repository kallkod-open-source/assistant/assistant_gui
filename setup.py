import pathlib
from setuptools import setup

HERE = pathlib.Path(__file__).parent
REQUIREMENTS = (HERE / "requirements.txt").read_text().splitlines()


if __name__ == "__main__":
    setup(
        name='assistant_gui',
        version='0.2.0.dev1',
        packages=['assistant_gui'],
        package_data={'assistant_gui': ['main.css']},
        install_requires=REQUIREMENTS,
        python_requires='==3.8.10',
    )
