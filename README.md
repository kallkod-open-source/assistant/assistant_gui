# installation of assistant-gui

## Prerequisites
Before installation of `assistant-gui` package, you need to have [assistant](https://gitlab.com/kallkod-assistant/assistant) and [assistant-rpc](https://gitlab.com/kallkod-assistant/assistant-rpc) packages installed. You will use the same virtual environment, which was created for `assitant`.


## How to install assistant-gui:

```bash
git clone git@gitlab.com:kallkod-assistant/assistant-gui.git
source ~/Work/virtualenvs/env/bin/activate  # activate existing virtual environment
cd ~/Work/assistant-gui/  # cd to cloned package directory
pip install -e .  # install package in editable state, for develop
# Requirements should be installed automatically during package installation.
# If it doesn't, run following command in terminal:
# pip3 install -r requirements.txt  # install dependencies
```


## How to run GUI:

```bash
# 1) open the terminal
# 2) activate vitual environment:
source ~/Work/virtualenvs/env/bin/activate

# 3) cd to your local copy of 'assistant' repository, to the 'grpc_server' directory
cd ~/Work/assistant/assistant/grpc_server

# 4) run gRPC server:
python3 server.py

# 5) open new tab in terminal
# 6) activate virtual environment (the same used in step 2):
source ~/Work/virtualenvs/env/bin/activate

# 7) cd to assistant-gui :
cd ~/Work/assistant-gui/assistant_gui/

# 8) run the GUI
python3 assistant_app.py
```

## Version numbers
Version numbers in `setup.py` and git tags should follow the same rules as `assistant` project.
