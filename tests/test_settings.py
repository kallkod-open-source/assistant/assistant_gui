from unittest import TestCase
import tempfile
from pathlib import Path

from assistant_gui import settings

class SettingsTests(TestCase):
    def setUp(self):
        self.file_name = Path(tempfile.gettempdir()) / "settings.json"
        self.assertFalse(self.file_name.is_file())

    def tearDown(self):
        self.file_name.unlink(missing_ok=True)
        pass 

    def test_create(self):
        s = settings.Config(self.file_name)
        self.assertEqual(s.most_recent_records, settings.DefaultConfig.most_recent_records)

    def test_exists(self):
        s = settings.Config(self.file_name)
        s.most_recent_records = 25
        s.save()

        m = settings.Config(self.file_name)
        self.assertEqual(s.most_recent_records, m.most_recent_records)
        
