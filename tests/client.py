import logging
import grpc
from google.protobuf import empty_pb2

from assistant_rpc import assistant_pb2
from assistant_rpc import assistant_pb2_grpc


class AssistantClient():
    def __init__(self):
        self.channel = grpc.insecure_channel('localhost:5005')
        self.stub = assistant_pb2_grpc.AssistServerStub(self.channel)

    def close(self):
        self.channel.close()

    def get_user_list(self):
        response = self.stub.GetUserList(empty_pb2.Empty())

        print("List of users:")
        for u in response.users:
            print(u.name, end=", ")

        print("\n\nDone")

    def create_user(self):
        user = assistant_pb2.User()
        user.name = 'Petri'
        response = self.stub.CreateUser(user)

        print('User created')

    def process_user_input(self, text, count):
        user_input = assistant_pb2.UserInput()
        user_input.user = 'ego'
        user_input.text = text
        user_input.count = count

        if count < 0:
            raise Exception("count should be more than 0")

        response = self.stub.ProcessUserInput(user_input)

        print('Message processed:\n', '-'*10)
        for elem in response.rows:
            debug_info = f'date: {elem.date} \nmessage id: {elem.message_id} \nmessage: {elem.text} \ndeadline: {elem.deadline}'
            print(debug_info)
            print('\n')
        print(response.status)
        
    
    def latest_recorded_messages(self, user, count):
        latest_msg_request = assistant_pb2.LatestMsgsRequest()
        latest_msg_request.user = user
        latest_msg_request.count = count
        response = self.stub.LatestRecordedMessages(latest_msg_request)

        print('Latest recorded messages:\n', '-'*10)
        for elem in response.rows:
            debug_info = f'date: {elem.date} \nmessage id: {elem.message_id} \nmessage: {elem.text} \ndeadline: {elem.deadline}'
            print(debug_info)
            print('\n')
        print(response.status)

        # return response.rows

    def get_qa(self, text):
        user_input = assistant_pb2.UserInput()
        user_input.text = text
        response = self.stub.GetTypeQA(user_input)
        if response.action == assistant_pb2.Type.READ:
            print(f"'{text}' QA TYPE: READ")
            print(response.status, "\n")
        if response.action == assistant_pb2.Type.WRITE:
            print(f"'{text}' QA TYPE: WRITE")
            print(response.status, "\n")


if __name__ == '__main__':
    logging.basicConfig()
    client = AssistantClient()

    # client.get_user_list()
    # client.create_user()
    client.process_user_input('I want to buy pink pony', 3)
    # client.process_user_input('I need to decide what to do', 4)
    # client.latest_recorded_messages("ego", 3)

    # client.get_qa("I need to go home in 5 minutes")
    # client.get_qa("What to do")
    # client.get_qa("I need to buy new flowers at 17:05")
    # client.get_qa("ASDFOL:KJ;lKHLOGFSLKJuhFSDGLIHUSFKGJ")
    # client.get_qa("sdgloihIYRuytrUT:;pOIY;opi\":POGEdfgdf")
    # client.get_qa(" ")
    # client.get_qa("")

    client.close()
